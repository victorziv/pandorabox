"""Setup script for pandorabox."""
from __future__ import print_function
from setuptools import setup
from setuptools.command.test import test as TestCommand
import codecs
import os
import sys

HERE = os.path.abspath(os.path.dirname(__file__))
# ______________________________


def read(*parts):
    """Return multiple read calls to different readable objects as a single
    string."""
    # intentionally *not* adding an encoding option to open
    return codecs.open(os.path.join(HERE, *parts), 'r').read()
# ______________________________


LONG_DESCRIPTION = read('README.rst')
# =============================


class PyTest(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)

        self.test_args = [
            '--strict',
            '--verbose',
            '--tb=long',
            'pandorabox/tests']

        self.test_suite = True
    # ___________________________

    def run_tests(self):
        import pytest
        errno = pytest.main(self.test_args)
        sys.exit(errno)
# ==============================


setup(
    name='pandorabox',
    version=read('version.txt').strip(),
    url='https://victorziv@bitbucket.org/victorziv/pandorabox.git',
    license='Apache Software License',
    author='Victor Ziv',
    tests_require=['pytest', 'pytest-cov'],

    install_requires=[
        'Flask>=0.12.2',
        'Flask-Login>=0.4.0',
        'Flask-Script>=2.0.5',
        'gunicorn>=19.7.1',
        'psycopg2>=2.7.1',
        'pytest>=3.1.2',
        'Sphinx>=1.6.2',
        'sphinxcontrib-websupport>=1.0.1',
    ],

    cmdclass={'test': PyTest},

    author_email='ziv.victor@gmail.com',
    description='The best sea food ever',
    long_description=LONG_DESCRIPTION,
    entry_points={
        'console_scripts': [
            'pandorabox = pandorabox.__main__:main',
        ],
    },
    packages=['pandorabox'],
    include_package_data=True,
    platforms='any',
    test_suite='tests.test_pandorabox',
    zip_safe=False,
    package_data={'pandorabox': ['templates/**.html']},
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 3',
        'Development Status :: 4 - Beta',
        'Natural Language :: English',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: OS Independent',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Software Development :: Libraries :: Application Frameworks',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
