#!/bin/bash

NAME={{ APP_NAME }}
curdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECTDIR=$(dirname ${curdir})
SOCKFILE=${PROJECTDIR}/run/${NAME}.sock
USER=ivt
GROUP=ivt
NUM_WORKERS=2

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
source ${PROJECTDIR}/venv/bin/activate

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

$(which gunicorn) manage:app --name ${NAME} --workers ${NUM_WORKERS} --user ${USER} --bind=unix:${SOCKFILE}

