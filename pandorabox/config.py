import sys
import os
import logging
logger = None
# ===================================


class Config:
    PROJECT = 'pandorabox'
    BASEDIR = os.path.abspath(os.path.dirname(__file__))
    LOGPATH = os.path.join(os.path.dirname(BASEDIR), 'logs')
    API_URL_PREFIX = '/api/v1.0'
    PROJECT_USER = 'pandora'
    SECRET_KEY = os.environ.get('SECRET_KEY') \
        or 'il y a beaucoup de circulation dans les rues'
    ADMIN_USER = os.environ.get('ADMIN_USER') \
        or '%s@nowhere.com' % PROJECT_USER

    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USE_TLS = True
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')

    MAIL_SUBJECT_PREFIX = '[%s]' % PROJECT.capitalize()
    MAIL_SENDER = 'Admin %s' % ADMIN_USER
    TEST_OWNER_DEFAULT = '%s@nowhere.com' % PROJECT_USER

    DBHOST = 'localhost'
    DBPORT = 5432
    DBUSER = PROJECT_USER
    DBPASSWORD = PROJECT_USER

    DB_CONNECTION_PARAMS = dict(
        dbhost=DBHOST,
        dbport=DBPORT,
        dbuser=DBUSER,
        dbpassword=DBPASSWORD
    )

    DB_URI_FORMAT = 'postgresql://{dbuser}:{dbpassword}@{dbhost}:{dbport}/{dbname}'
    DBNAME_ADMIN = 'postgres'
    DB_CONN_URI_ADMIN = DB_URI_FORMAT.format(
        dbname=DBNAME_ADMIN,
        **DB_CONNECTION_PARAMS
    )

    AUTH_TOKEN_EXPIRATION = 3600
    LOGGING_FORMAT = ' '.join([
        '%(asctime)s',
        '%(levelname)s',
        '%(process)d',
        '%(module)s',
        '%(funcName)s',
        '%(lineno)d'
    ])

    LOGGING_FORMAT += '\t%(message)s'

    OAUTH_CREDENTIALS = {
        "facebook": {},
        "twitter": {
            "id": "zwY4WSnGxv9nLz0RZnBsMpQPH",
            "secret": "wiekiIXApl03P47xGSLFezJb6rzTvQdEL4mQJ3Fim7fNJNN344"
        },
        "google": {
            "id": "261888576370-pn751o1qrqbcu662nmr3a24nr9cf3eku.apps.googleusercontent.com",
            "secret": "TDwCUl-WzhOjGw6x_P5bYxtH"
        }
    }

    MIGRATIONS_DIR = "migrations"
    MIGRATIONS_MODULE = "migrations"

    # ___________________________________________

    @staticmethod
    def create_directory(path):
        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except OSError as e:
                if e.errno == 17:
                    pass
    # _________________________________

    @classmethod
    def set_logging(cls, name, session='', loglevel='DEBUG', console_logging=False, console_loglevel='INFO'):

        global logger
        logger = logging.getLogger(name)
        logger.setLevel(getattr(logging, loglevel.upper()))
        logger.propogate = False
        logfile = os.path.join(Config.LOGPATH, session, '%s.log' % name)
        logger.addHandler(Config.log_to_file(name, session, logfile, loglevel))

        if console_logging:
            logger.addHandler(Config.log_to_console(console_loglevel))
            logger.info("Log path: %s", logfile)

    # ________________________________________

    @staticmethod
    def log_to_console(logging_level, out_to='stderr'):
        logformat = '%(asctime)s - %(levelname)-10s %(message)s'
        handler = logging.StreamHandler(getattr(sys, out_to))
        handler.setLevel(getattr(logging, logging_level.upper()))
        handler.setFormatter(logging.Formatter(logformat))
        return handler
    # ___________________________________________

    @staticmethod
    def log_to_file(name, session, logfile, logging_level):
        logformat = '%(asctime)s %(name)s %(process)d %(levelname)-10s %(module)s %(funcName)-4s %(message)s'
        Config.create_directory(os.path.dirname(logfile))

        handler = logging.FileHandler(logfile)

        handler.setLevel(getattr(logging, logging_level.upper()))
        handler.setFormatter(logging.Formatter(logformat))

        return handler
    # ___________________________________________

    @classmethod
    def init_app(cls, app, logging_name):
        pass
# ===================================


class DevelopmentConfig(Config):
    DEBUG = True
    DBNAME = "%sdev" % Config.PROJECT
    DB_CONN_URI = Config.DB_URI_FORMAT.format(
        dbname=DBNAME,
        **Config.DB_CONNECTION_PARAMS
    )

    # ________________________________

    @classmethod
    def init_app(cls, app, logging_name):
        pass
# ===================================


class TestingConfig(Config):
    APPTYPE = 'testing'
    TESTING = True
    SERVER_NAME = 'localhost'
    DBNAME = "%stest" % Config.PROJECT
    DB_CONN_URI = Config.DB_URI_FORMAT.format(
        dbname=DBNAME,
        **Config.DB_CONNECTION_PARAMS
    )

    @classmethod
    def init_app(cls, app, logging_name):
        pass

# ===================================


class ProductionConfig(Config):

    DBNAME = Config.PROJECT
    DB_CONN_URI = Config.DB_URI_FORMAT.format(
        dbname=DBNAME,
        **Config.DB_CONNECTION_PARAMS
    )
    # _____________________________

    @classmethod
    def init_app(cls, app):

        # email errors to the administrator
        from logging.handlers import SMTPHandler
        credentials = None
        secure = None
        if getattr(cls, 'MAIL_USERNAME', None) is not None:
            credentials = (cls.MAIL_USERNAME, cls.MAIL_PASSWORD)

        if getattr(cls, 'MAIL_USE_TLS', None):
            secure = ()

        mail_handler = SMTPHandler(
            mailhost=(cls.MAIL_SERVER, cls.MAIL_PORT),
            fromaddr=cls.IVT_MAIL_SENDER,
            toaddrs=[cls.IVT_ADMIN],
            subject='%s Application Error' % cls.IVT_MAIL_SUBJECT_PREFIX,
            credentials=credentials, secure=secure
        )

        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)

# ===================================


config = {
    'develop': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
# ___________________________________
