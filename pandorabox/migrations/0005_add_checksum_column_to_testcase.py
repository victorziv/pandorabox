#!/usr/bin/env python


def create_table_testcase(conn):
    cursor = conn.cursor()

    query = """
        ALTER TABLE IF EXISTS testcase
        ADD COLUMN IF NOT EXISTS checksum CHAR(32)
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_testcase(conn):
    cursor = conn.cursor()

    query = """
        ALTER TABLE IF EXISTS testcase
        DROP COLUMN IF EXISTS checksum
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def upgrade(conn, **kwargs):
    create_table_testcase(conn)
# _______________________________


def downgrade(conn):
    drop_table_testcase(conn)
