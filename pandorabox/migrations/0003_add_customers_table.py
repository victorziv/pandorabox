#!/usr/bin/env python


def alter_machines_add_column_customerid(conn):
    query = """
        ALTER TABLE machines
        ADD COLUMN IF NOT EXISTS customerid INTEGER,
        ADD FOREIGN KEY (customerid) REFERENCES customers (id) ON DELETE CASCADE
    """
    params = ()
    cursor = conn.cursor()
    cursor.execute(query, params)
    conn.commit()
# _____________________________


def create_table_customers(conn):
    cursor = conn.cursor()

    query = """
        CREATE TABLE IF NOT EXISTS customers (
            id serial PRIMARY KEY,
            name VARCHAR(64) UNIQUE,
            web VARCHAR(256)
        );
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def drop_table_customers(conn):
    cursor = conn.cursor()

    query = """
        DROP TABLE IF EXISTS customer;
    """
    params = {}

    cursor.execute(query, params)
    conn.commit()
# _____________________________


def alter_machines_drop_column_customerid(conn, **kwargs):
    query = """
        ALTER TABLE machines
        DROP COLUMN IF EXISTS customerid CASCADE
    """
    params = ()
    cursor = conn.cursor()
    cursor.execute(query, params)
    conn.commit()
# _______________________________


def upgrade(conn, **kwargs):
    create_table_customers(conn)
    alter_machines_add_column_customerid(conn)
# _______________________________


def downgrade(conn):
    drop_table_customers(conn)
    alter_machines_drop_column_customerid(conn)
