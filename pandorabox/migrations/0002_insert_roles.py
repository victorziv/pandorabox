#!/usr/bin/env python
from flask import current_app
from models import Role


def upgrade(conn, **kwargs):
    app_context = current_app.app_context()
    app_context.push()
    Role.insert_roles()
    app_context.pop()

# _______________________________


def downgrade(conn, **kwargs):
    pass
