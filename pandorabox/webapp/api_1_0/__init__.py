from flask import Blueprint
api = Blueprint('api', __name__)
from . import machines  # noqa
from . import testcases # noqa
from . import ivtdoc  # noqa
