colModel: [
    {
        label: 'Case ID',
        name: 'caseid',
        index: 'caseid',
        align: 'centre',
        sortable: true,
        hidden: true,
        search: true
    },
    {
        label: 'Name',
        name: 'name',
        index: 'name',
        align: 'centre',
        sortable: true,
        hidden: false,
        search: true
    },
    {
        label: 'Owner',
        name: 'owner',
        index: 'owner',
        align: 'centre',
        sortable: true,
        hidden: false,
        search: true
    },
    {
        label: 'Author',
        name: 'author',
        index: 'author',
        align: 'centre',
        sortable: true,
        hidden: false,
        search: true
    },
    {
        label: 'Category',
        name: 'category',
        index: 'category',
        align: 'centre',
        sortable: true,
        hidden: false,
        search: true
    },
    {
        label: 'Created',
        name: 'created',
        index: 'created',
        align: 'centre',
        sortable: true,
        hidden: false,
        search: true
    }
],
