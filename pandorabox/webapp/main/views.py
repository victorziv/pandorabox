from flask import render_template
from flask import current_app as cap
from . import main
from config import logger
# ______________________________


@main.route('/', methods=['GET'])
@main.route('/ivts', methods=['GET'])
def ivts():
    return render_template(
        "main/ivts/ivts.html",
        config=cap.config,
    )
# ______________________________


@main.route('/shipped/', methods=['GET'])
def shipped():
    return render_template(
        "main/shipped/shipped.html",
        config=cap.config,
    )
# ______________________________


@main.route('/machines/', methods=['GET'])
def machines():
    template_path = "main/machines/machines.html"
    logger.debug("Template path: %s", template_path)
    return render_template(template_path, config=cap.config)
# ______________________________


@main.route('/testcases/', methods=['GET'])
def testcases():
    return render_template(
        "main/testcases/testcases.html",
        config=cap.config,
    )
# ______________________________
